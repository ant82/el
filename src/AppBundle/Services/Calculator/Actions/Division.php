<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:31
 */

namespace AppBundle\Services\Calculator\Actions;


use AppBundle\Services\Calculator\ActionInterface;

class Division implements ActionInterface
{

    /**
     * @param array $values
     * @return integer
     */
    public function calc($values)
    {
        $result = (int)array_shift($values);

        return $result / array_product($values);
    }

    /**
     * @return integer
     */
    public function getPriority()
    {
        return 2;
    }
}