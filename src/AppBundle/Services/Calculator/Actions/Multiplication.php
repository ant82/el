<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:29
 */

namespace AppBundle\Services\Calculator\Actions;


use AppBundle\Services\Calculator\ActionInterface;

class Multiplication implements ActionInterface
{

    /**
     * @param array $values
     * @return integer
     */
    public function calc($values)
    {
        return array_product($values);
    }
    /**
     * @return integer
     */
    public function getPriority()
    {
        return 1;
    }
}