<?php
namespace AppBundle\Services\Calculator\Actions;

use AppBundle\Services\Calculator\ActionInterface;

/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:23
 */
class Sum implements ActionInterface
{

    /**
     * @param array $values
     * @return integer
     */
    public function calc($values)
    {
        return array_sum($values);
    }
    /**
     * @return integer
     */
    public function getPriority()
    {
        return 3;
    }
}