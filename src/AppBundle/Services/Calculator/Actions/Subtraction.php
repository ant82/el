<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:25
 */

namespace AppBundle\Services\Calculator\Actions;


use AppBundle\Services\Calculator\ActionInterface;

class Subtraction implements ActionInterface
{

    /**
     * @param array $values
     * @return integer
     */
    public function calc($values)
    {
        $result = (int)array_shift($values);

        foreach ($values as $value) {
            $result -= (int)$value;
        }

        return $result;
    }

    /**
     * @return integer
     */
    public function getPriority()
    {
        return 4;
    }
}