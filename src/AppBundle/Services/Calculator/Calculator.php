<?php
namespace AppBundle\Services\Calculator;
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:13
 */
class Calculator
{
    /**
     * @var ActionInterface
     */
    private $action;
    /**
     * @var array
     */
    private $values = [];

    /**
     * @param integer $value
     */
    public function addValue($value)
    {
        $this->values[] = (int)$value;
    }

    /**
     * @param ActionInterface $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    public function calculate()
    {
        return $this->action->calc($this->values);
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

}