<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 21.09.2016
 * Time: 12:01
 */

namespace AppBundle\Services\Calculator;


class CalculationStringParser
{

    /**
     * массив символов математических операций для расчета
     * @var array
     */
    private $actions = [];

    /**
     *  массив чисел для расчета
     * @var array
     */
    private $values = [];

    /**
     *  массив ошибок
     * @var string
     */
    private $error = '';


    /**
     * @param string $expression
     * @return bool
     */
    public function setExpression($expression)
    {
        preg_match_all('/[^0-9*\-+\/]/', $expression, $badChars);
        if (!empty($badChars[0])) {
            $this->error = sprintf('Недопустимые символы в строке для расчета %s', implode(',', $badChars[0]));

            return false;
        }
        $expression = preg_replace('/[^0-9*\-+\/]/', '', $expression);
        preg_match_all("/[^0-9]/", $expression, $this->actions);
        preg_match_all("/[0-9]+/", $expression, $this->values);
        $this->values = array_shift($this->values);
        if (count($this->values) < 2) {
            $this->error = 'Недостаточно цифр для расчета';

            return false;
        }
        $this->actions = array_shift($this->actions);

        return true;
    }


    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

}