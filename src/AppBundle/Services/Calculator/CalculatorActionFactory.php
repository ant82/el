<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 21.09.2016
 * Time: 11:58
 */

namespace AppBundle\Services\Calculator;

use AppBundle\Services\Calculator\ActionInterface;
use AppBundle\Services\Calculator\Actions\Division;
use AppBundle\Services\Calculator\Actions\Multiplication;
use AppBundle\Services\Calculator\Actions\Subtraction;
use AppBundle\Services\Calculator\Actions\Sum;

class CalculatorActionFactory
{
    private $validActions = [
        '+' => Sum::class,
        '-' => Subtraction::class,
        '*' => Multiplication::class,
        '/' => Division::class
    ];


    public function getActionByMathChar($char)
    {
        if (isset($this->validActions[$char])) {
            return new $this->validActions[$char];
        }

        return null;
    }


}