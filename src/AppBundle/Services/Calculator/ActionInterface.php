<?php
/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:17
 */

namespace AppBundle\Services\Calculator;


interface ActionInterface
{
    /**
     * @param array $values
     * @return integer
     */
    public function calc($values);

    /**
     * @return integer
     */
    public function getPriority();
}