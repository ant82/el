<?php

namespace AppBundle\Controller;

use AppBundle\Services\Calculator\ActionInterface;
use AppBundle\Services\Calculator\Actions\Division;
use AppBundle\Services\Calculator\Actions\Multiplication;
use AppBundle\Services\Calculator\Actions\Subtraction;
use AppBundle\Services\Calculator\Actions\Sum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function IndexAction()
    {
        return $this->render('AppBundle:Index:index.html.twig', array(// ...
        ));
    }

    /**
     * @Route("/calc/",name="calc_route")
     * @param Request $request
     * @return JsonResponse
     */
    public function calcAction(Request $request)
    {
        $calculator        = $this->get('calculator');
        $calcActionFactory = $this->get('calc_action_factory');
        $calcStringParser  = $this->get('calc_string_parser');

        /* строка из фронта для расчета (напр.2+2)*/
        $expression = $request->query->get('expr');

        $errors = [];
        $r      = null;

        if (!$calcStringParser->setExpression($expression)) {
            $errors[] = $calcStringParser->getError();
        }

        $actions = $calcStringParser->getActions();
        $values  = $calcStringParser->getValues();

        if (empty($errors)) {
            $calculator->setValues([$values[0], $values[1]]);
            /*
             * Пока возьмем только одно (первое) требуемое математическое действие.
             * В дальнейшем можно доработать решение для выполнения нескольких действий в рамках
             * одного запроса с учетом правил порядка (умножение/деление - затем сложением вычитание)
             */
            if (count($actions) > 1) {
                $actions  = [array_shift($actions)];
                $errors[] = sprintf('Выполнение нескольих действий сразу пока не поддерживается. Выполнено только %s', $actions[0]);
            }

            foreach ($actions as $actionChar) {
                $actionObj = $calcActionFactory->getActionByMathChar($actionChar);
                if ($actionObj instanceof ActionInterface) {
                    $calculator->setAction($actionObj);
                    $r = $calculator->calculate();
                } else {
                    $errors[] = sprintf('Операция %s не поддерживается', $actionChar);
                }
            }
        }


        $response = new JsonResponse(['result' => $r, 'errors' => $errors, 'status' => 1]);
        if ($callback = $request->get('callback')) {
            $response->setCallback($callback);
        }

        return $response;
    }

}
