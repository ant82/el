<?php
use AppBundle\Services\Calculator\CalculationStringParser;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Created by PhpStorm.
 * User: writer
 * Date: 21.09.2016
 * Time: 12:45
 */
class CalculatorStringParserTest extends KernelTestCase
{

    public function testParseString()
    {
        $calcParser = new CalculationStringParser();
        $calcParser->setExpression('2+2');
        $this->assertEquals([2, 2], $calcParser->getValues());
        $this->assertEquals(['+'], $calcParser->getActions());
        $calcParser->setExpression('11*892');
        $this->assertEquals([11, 892], $calcParser->getValues());
        $this->assertEquals(['*'], $calcParser->getActions());
        $calcParser->setExpression('11*892-800+9');
        $this->assertEquals([11, 892, 800, 9], $calcParser->getValues());
        $this->assertEquals(['*', '-', '+'], $calcParser->getActions());
    }

}