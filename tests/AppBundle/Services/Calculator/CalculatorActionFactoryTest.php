<?php
use AppBundle\Services\Calculator\Actions\Division;
use AppBundle\Services\Calculator\Actions\Multiplication;
use AppBundle\Services\Calculator\Actions\Subtraction;
use AppBundle\Services\Calculator\Actions\Sum;
use AppBundle\Services\Calculator\CalculatorActionFactory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Created by PhpStorm.
 * User: writer
 * Date: 21.09.2016
 * Time: 12:53
 */
class CalculatorActionFactoryTest extends KernelTestCase
{
    public function testGetActionByMathActionChar()
    {
        $caf = new CalculatorActionFactory();
        $this->assertInstanceOf(Sum::class, $caf->getActionByMathChar('+'));
        $this->assertInstanceOf(Subtraction::class, $caf->getActionByMathChar('-'));
        $this->assertInstanceOf(Multiplication::class, $caf->getActionByMathChar('*'));
        $this->assertInstanceOf(Division::class, $caf->getActionByMathChar('/'));
        $this->assertNull($caf->getActionByMathChar('$'));
    }
}