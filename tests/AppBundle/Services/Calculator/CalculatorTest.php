<?php

use AppBundle\Services\Calculator\Actions\Division;
use AppBundle\Services\Calculator\Actions\Multiplication;
use AppBundle\Services\Calculator\Actions\Subtraction;
use AppBundle\Services\Calculator\Actions\Sum;
use AppBundle\Services\Calculator\Calculator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Created by PhpStorm.
 * User: writer
 * Date: 20.09.2016
 * Time: 17:36
 */
class CalculatorTest extends WebTestCase
{

    public function testSumOperation()
    {
        $calc = new Calculator();
        $calc->setAction(new Sum());
        $calc->setValues([2, 5]);
        $this->assertEquals(7, $calc->calculate());
        $calc->setValues([2, 5,197]);
        $this->assertEquals(204, $calc->calculate());
    }

    public function testDivisionOperation()
    {
        $calc = new Calculator();
        $calc->setAction(new Division());
        $calc->setValues([2, 5]);
        $this->assertEquals(0.4, $calc->calculate());
        $calc->setValues([2, 5,197]);
        $this->assertEquals(0.002, round($calc->calculate(),3));
    }
    public function testMultiplicationOperation()
    {
        $calc = new Calculator();
        $calc->setAction(new Multiplication());
        $calc->setValues([2, 53]);
        $this->assertEquals(106, $calc->calculate());
        $calc->setValues([2, 5,197]);
        $this->assertEquals(1970, $calc->calculate());
    }
    public function testSubtractionOperation()
    {
        $calc = new Calculator();
        $calc->setAction(new Subtraction());
        $calc->setValues([2, 53]);
        $this->assertEquals(-51, $calc->calculate());
        $calc->setValues([2999, 5,197]);
        $this->assertEquals(2797, $calc->calculate());
    }

}